<?php
/**
 * Created by PhpStorm.
 * User: zhc
 * Date: 2019/10/24
 * Time: 15:14
 */

namespace Logic;


use think\Db;

class Visitor
{
    public static function addCustomer($data)
    {
        return Db::name('visitor')->insert($data);

    }
    public  static  function updateCustomer($data){
        $info = Db::name('visitor')->where('visitor_id', $data['visitor_id'])->find();
        if(!empty($info)) {
          return  Db::name('visitor')->where('visitor_id', $data['visitor_id'])->update($data);
        }else {

            return self::addCustomer($data);
        }
    }

    public  static  function getCustomerInfoOnlineByCustomerId($visitor_id){
        $info =Db::name('visitor')->where('visitor_id', $visitor_id)->where('online_status',1)->find();
        return $info;

    }
    public  static  function getCustomerInfoCustomerId($visitor_id){
        $info =Db::name('visitor')->where('visitor_id', $visitor_id)->find();
        return  $info;

    }
    public  static  function  setOnline($visitor_id,$status,$fd=0){
        $update['online_status'] =$status;
        $update['client_id'] =$fd;
        return  Db::name('visitor')->where('visitor_id', $visitor_id)->update($update);
    }


    /**
     * 查找游客列表
     * @return boolean
     */
    public  static  function  getVisitorList($limit=10){
        return Db::name('visitor')->order('online_status,create_time desc')->paginate($limit);

    }

    /**
     * 获取服务记录
     * @return boolean
     */
    public  static  function  getServiceList($where=[],$limit=10){
        return Db::name('visitor_service_log')->where($where)->order('vsid desc')->paginate($limit);
    }
    /**
     * 获取游客总数
     * @return boolean
     */
    public  static function  visitorCount($where=[]){
        return Db::name('visitor')->where($where)->count();
    }
}
